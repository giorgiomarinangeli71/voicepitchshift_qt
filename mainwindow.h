#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "src/RtAudio.h"

namespace Ui {
class MainWindow;
}

class PitShift;
//class RtAudio;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    PitShift* pPitShift[2];

private slots:
    void on_pitchDial_valueChanged(int value);

    void on_mixSlider_valueChanged(int value);

    void on_resetPushButton_clicked();

    void on_startPushButton_clicked();

public slots:

    void on_closeInfoPushButton_clicked();

    void on_openInfoPushButton_clicked();

private:
    Ui::MainWindow *ui;

    RtAudio *adac;

    QVector <RtAudio::DeviceInfo> devInfo;

    void setShift(float val);
    void OpenAudioPort(int,int);

};

#endif // MAINWINDOW_H
