#-------------------------------------------------
#
# Project created by QtCreator 2017-09-10T21:38:30
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = VoicePitchShift
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += ./asio

linux*:!simulator {

    DEFINES += __LINUX_ALSA__
    DEFINES += __LINUX_ALSASEQ__
    DEFINES += AVOID_TIMESTAMPING

    CONFIG += link_pkgconfig x11

    jack_midi {
        PKGCONFIG += jack
        DEFINES += __LINUX_JACK__
    }

    PKGCONFIG += alsa
    LIBS += -lpthread
    LIBS += -lasound
}

win32 {

    DEFINES += __WINDOWS_ASIO__
    #correct method:
    INCLUDEPATH += "$$PWD/C:\Windows\System32"

    LIBS += -lwinmm
    LIBS += -lws2_32
    LIBS += -lole32
}

macx {
    DEFINES += __MACOSX_CORE__

    LIBS += -framework \
        CoreMidi \
        -framework \
        CoreAudio \
        -framework \
        CoreFoundation \
}

macx {
SOURCES += \
        main.cpp \
        mainwindow.cpp \
        stk/BiQuad.cpp \
        stk/Delay.cpp \
        stk/DelayL.cpp \
        stk/Filter.cpp \
        stk/PitShift.cpp \
        stk/Stk.cpp \
        src/RtAudio.cpp \


HEADERS += \
        mainwindow.h \
        stk/BiQuad.h \
        stk/Delay.h \
        stk/DelayL.h \
        stk/Filter.h \
        stk/PitShift.h \
        stk/Stk.h \
        src/RtError.h \
        src/RtAudio.h \
}

win32 {
SOURCES += \
        main.cpp \
        mainwindow.cpp \
        asio/asio.cpp \
        asio/asiodrivers.cpp \
        asio/asiolist.cpp \
        asio/iasiothiscallresolver.cpp \
        stk/BiQuad.cpp \
        stk/Delay.cpp \
        stk/DelayL.cpp \
        stk/Filter.cpp \
        stk/PitShift.cpp \
        stk/Stk.cpp \
        src/RtAudio.cpp \


HEADERS += \
        mainwindow.h \
        asio/asio.h \
        asio/asiodrivers.h \
        asio/asiodrvr.h \
        asio/asiolist.h \
        asio/asiosys.h \
        asio/dsound.h \
        asio/FunctionDiscoveryKeys_devpkey.h \
        asio/ginclude.h \
        asio/iasiodrv.h \
        asio/iasiothiscallresolver.h \
        asio/soundcard.h \
        stk/BiQuad.h \
        stk/Delay.h \
        stk/DelayL.h \
        stk/Filter.h \
        stk/PitShift.h \
        stk/Stk.h \
        src/RtError.h \
        src/RtAudio.h \
}


FORMS += \
        mainwindow.ui
