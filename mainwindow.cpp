/**
 *  @file    mainwindow.cpp
 *  @author  Giorgio Marinangeli
 *  @date    10/08/2017
 *  @version 1.0
 *
 *  @brief Test program for voice real time pitch shifting.
 *
 *  @section DESCRIPTION
 *
 *
 */
//---------------------------------------------------------------------------------------------------------------------------------------
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QMessageBox>
#include "stk/PitShift.h"
#include "src/RtAudio.h"
//---------------------------------------------------------------------------------------------------------------------------------------
static int myAudioInOutCallback(void *outputBuffer, void *inputBuffer, unsigned int nBufferFrames,double streamTime, RtAudioStreamStatus status, void *userData )
{

    if ( status )
        qDebug() << "Stream over/underflow detected.";

    float* pOutBuffer = (float*)outputBuffer;
    float* pInBuffer  = (float*)inputBuffer;

    MainWindow* p = (MainWindow*)userData;

    for ( int i=0; i<(nBufferFrames*2); i+=2 ) {
        for(int ch=0;ch<2;ch++){
            pOutBuffer[i+ch]  = p->pPitShift[ch]->tick(pInBuffer[i+ch]);
        }
    }

    return 0;
}

//-------------------------------------------------------------------------------------------------------------------------
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->actionInfo_2, SIGNAL(triggered()),this,SLOT(on_openInfoPushButton_clicked()));

    pPitShift[0] = new PitShift();
    pPitShift[1] = new PitShift();

    pPitShift[0]->setShift(2.0);
    pPitShift[1]->setShift(2.0);

    pPitShift[0]->setEffectMix(1.0);
    pPitShift[1]->setEffectMix(1.0);

    RtAudio audio;
    // Determine the number of devices available
    unsigned int devices = audio.getDeviceCount();
    // Scan through devices for various capabilities
    RtAudio::DeviceInfo info;

    int inIdx = -1;
    int outIdx = -1;

    for ( unsigned int i=0; i<devices; i++ ) {

        info = audio.getDeviceInfo( i );

        devInfo.append(info);

        qDebug() << "-----------------------";
        qDebug() << "probed:"           << info.probed;                 /*!< true if the device capabilities were successfully probed. */
        qDebug() << "name:"             << QString(info.name.c_str());  /*!< Character string device identifier. */
        qDebug() << "outputChannels:" << info.outputChannels;   /*!< Maximum output channels supported by device. */
        qDebug() << "inputChannels:" << info.inputChannels;     /*!< Maximum input channels supported by device. */
        qDebug() << "duplexChannels:" << info.duplexChannels;   /*!< Maximum simultaneous input/output channels supported by device. */
        qDebug() << "isDefaultOutput:" << info.isDefaultOutput; /*!< true if this is the default output device. */
        qDebug() << "isDefaultInput:" << info.isDefaultInput;   /*!< true if this is the default input device. */

        for(int i=0;i<info.sampleRates.size();i++)
            qDebug() << "sampleRates:" << info.sampleRates.at(i); /*!< Supported sample rates (queried from list of standard rates). */

        qDebug() << "preferredSampleRate:"  << info.preferredSampleRate; /*!< Preferred sample rate, eg. for WASAPI the system sample rate. */
        qDebug() << "nativeFormats:"        << info.nativeFormats;  /*!< Bit mask of supported data formats. */


       if ( info.probed == true ) {

           QString str;

           //if(info.outputChannels){
                str = QString("%1 id:%2 inCh:%3 outCh:%4").arg(info.name.c_str()).arg(i).arg(info.inputChannels).arg(info.outputChannels);
                ui->outAudioComboBox->addItem(str);
           //}

           //if(info.inputChannels){
                //str = QString("%1 id:%2 inCh:%3 outCh:%4").arg(info.name.c_str()).arg(i).arg(info.inputChannels).arg(info.outputChannels);
                ui->inAudioComboBox->addItem(str);
           //}

           if(info.isDefaultInput){
               inIdx = i;
               qDebug() << "Default InId:" << i;
           }

           if(info.isDefaultOutput){
               outIdx = i;
               qDebug() << "Default OutId:" << i;
           }
       }
   }

   if( inIdx != -1 ){
       ui->inAudioComboBox->setCurrentIndex(inIdx);
   }

   if( outIdx != -1 ){
       ui->outAudioComboBox->setCurrentIndex(outIdx);
   }

   adac = new RtAudio();

   if ( adac->getDeviceCount() != 0 ) {
        OpenAudioPort(adac->getDefaultInputDevice(),adac->getDefaultOutputDevice());
   }
}
//-------------------------------------------------------------------------------------------------------------------------
MainWindow::~MainWindow()
{
    delete ui;

    adac->stopStream();
}

//---------------------------------------------------------------------------------------------------------------------------------------
void MainWindow::OpenAudioPort(int inId, int outId){

    if(adac != NULL){
        // Set the same number of channels for both input and output.
        unsigned int bufferFrames = 512;
        RtAudio::StreamParameters iParams, oParams;

        iParams.deviceId = inId; // first available device
        iParams.nChannels = devInfo.at(inId).inputChannels;

        qDebug() << "InId : " << inId << " InCh:" << devInfo.at(inId).inputChannels;

        oParams.deviceId = outId; // first available device
        oParams.nChannels = devInfo.at(outId).outputChannels;
        qDebug() << "OuId : " << outId << " OutCh:" << devInfo.at(outId).outputChannels;


        try {
         adac->openStream( &oParams, &iParams, RTAUDIO_FLOAT32, 44100, &bufferFrames, &myAudioInOutCallback, (void*) this );
         adac->startStream();
        }
        catch ( RtAudioError& e ) {

            QMessageBox::critical( this, tr("Error. Aborting"),QString::fromStdString(e.getMessage()));
        }
    }
}
//---------------------------------------------------------------------------------------------------------------------------------------
void MainWindow::on_pitchDial_valueChanged(int value)
{
    float shift = value/100.0;
    setShift(shift);
}
//---------------------------------------------------------------------------------------------------------------------------------------
void MainWindow::on_mixSlider_valueChanged(int value)
{
    for(int i=0;i<2;i++){
        pPitShift[i]->setEffectMix(value/100.0);
    }
}
//---------------------------------------------------------------------------------------------------------------------------------------
void MainWindow::on_resetPushButton_clicked()
{
    setShift(1.0);

    ui->pitchDial->blockSignals(true);
    ui->pitchDial->setValue((int)(100));
    ui->pitchDial->blockSignals(false);
}
//---------------------------------------------------------------------------------------------------------------------------------------
void MainWindow::setShift(float val){

    for(int i=0;i<2;i++){
        pPitShift[i]->setShift(val);
    }

    ui->pitchLabel->setText(QString("%1").arg(val));
}
//---------------------------------------------------------------------------------------------------------------------------------------
void MainWindow::on_closeInfoPushButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}
//---------------------------------------------------------------------------------------------------------------------------------------
void MainWindow::on_openInfoPushButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(1);
}
//---------------------------------------------------------------------------------------------------------------------------------------
void MainWindow::on_startPushButton_clicked()
{
    if(adac->isStreamRunning()){
        adac->stopStream();
    }else{
        adac->startStream();
    }
}
