/***************************************************/
/*! \class PitShift
    \brief STK simple pitch shifter effect class.

    This class implements a simple pitch shifter
    using delay lines.

    by Perry R. Cook and Gary P. Scavone, 1995 - 2002.
    
    
    MODIFIED by Stephane Beauchemin
    additions:
    - Longer delay lines
    - Let the users set the lenght of delays outside class.
*/
/***************************************************/
#include "PitShift.h"
#include <iostream>
#include <math.h>
//#include "OneZero.h"
PitShift :: PitShift()
{
	length 		= 1.0;
	delay[0] 	= 1012;
	delay[1] 	= 2512;
  
	maxDelay 	= 5024;
  delayLine[0] = new DelayL(delay[0], (long) maxDelay);
  delayLine[1] = new DelayL(delay[1], (long) maxDelay);
  delayLength = maxDelay - 12;
  //effectMix = (MY_FLOAT) 1.0;
	rate 		= 1.0;
  length 	= 1.0; //value from 0.1 to 1.0
}

PitShift :: ~PitShift()
{
  delete delayLine[0];
}

void PitShift :: clear()
{
  delayLine[0]->clear();
  lastOutput = 0.0;
}

void PitShift :: setEffectMix(MY_FLOAT mix)
{
  effectMix = mix;
  if ( mix < 0.0 ) {
    std::cerr << "PitShift: setEffectMix parameter is less than zero!" << std::endl;
    effectMix = 0.0;
  }
  else if ( mix > 1.0 ) {
    std::cerr << "PitShift: setEffectMix parameter is greater than 1.0!" << std::endl;
    effectMix = 1.0;
  }
}

void PitShift :: setShift(MY_FLOAT shift)
{
  if (shift < 1.0)    {
    rate = 1.0 - shift; 
   }
  else if (shift > 1.0)       {
    rate = 1.0 - shift;
  }
  else {
    rate = 0.0;
    delay[0] = 512;
  }
}

void PitShift :: setLength(MY_FLOAT nLength)//New function by Stephane Beauchemin
{
    length = nLength;
}

MY_FLOAT PitShift :: lastOut() const
{
  return lastOutput;
}

MY_FLOAT PitShift :: tick(MY_FLOAT input)
{
  delay[0] = delay[0] + rate;
  while (delay[0] > (delayLength+12)) delay[0] -= delayLength;
  while (delay[0] < 12) delay[0] += delayLength;
  
  delay[1] = delay[0] + (delayLength / 2);
  while (delay[1] > (delayLength+12)) delay[1] -= delayLength;
  while (delay[1] < 12) delay[1] += delayLength;
  
  delayLine[0]->setDelay((long)delay[0]);
  delayLine[1]->setDelay((long)delay[1]);
  
  env[1] = fabs((delay[0] - (delayLength / 2) + 12) * (1 / (delayLength / 2)));
  env[0] = 1.0 - env[1];
  
  lastOutput = env[0] * delayLine[0]->tick(input);
  lastOutput += env[1] * delayLine[1]->tick(input);
  lastOutput *= effectMix;
  lastOutput += (1.0 - effectMix) * input;
  delayLength = fabs((float)length * (maxDelay - 12));
  return lastOutput;
  
  
}

MY_FLOAT *PitShift :: tick(MY_FLOAT *vector, unsigned int vectorSize)
{
  for (unsigned int i=0; i<vectorSize; i++)
    vector[i] = tick(vector[i]);

  return vector;
}

